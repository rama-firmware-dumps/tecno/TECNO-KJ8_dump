#!/bin/bash

cat tr_product/priv-app/Messages/Messages.apk.* 2>/dev/null >> tr_product/priv-app/Messages/Messages.apk
rm -f tr_product/priv-app/Messages/Messages.apk.* 2>/dev/null
cat tr_product/app/Photos/Photos.apk.* 2>/dev/null >> tr_product/app/Photos/Photos.apk
rm -f tr_product/app/Photos/Photos.apk.* 2>/dev/null
cat tr_product/app/Gmail2/Gmail2.apk.* 2>/dev/null >> tr_product/app/Gmail2/Gmail2.apk
rm -f tr_product/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> product/priv-app/Velvet/Velvet.apk
rm -f product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/AICoreService/AICoreService.apk.* 2>/dev/null >> product/app/AICoreService/AICoreService.apk
rm -f product/app/AICoreService/AICoreService.apk.* 2>/dev/null
cat system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk.* 2>/dev/null >> system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk
rm -f system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk.* 2>/dev/null
cat system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null >> system_ext/app/TranssionCamera/TranssionCamera.apk
rm -f system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system/system/apex/com.android.btservices.apex.* 2>/dev/null >> system/system/apex/com.android.btservices.apex
rm -f system/system/apex/com.android.btservices.apex.* 2>/dev/null
