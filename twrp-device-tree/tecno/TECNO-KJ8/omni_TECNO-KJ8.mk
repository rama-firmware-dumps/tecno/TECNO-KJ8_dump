#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from TECNO-KJ8 device
$(call inherit-product, device/tecno/TECNO-KJ8/device.mk)

PRODUCT_DEVICE := TECNO-KJ8
PRODUCT_NAME := omni_TECNO-KJ8
PRODUCT_BRAND := TECNO
PRODUCT_MODEL := TECNO KJ8
PRODUCT_MANUFACTURER := tecno

PRODUCT_GMS_CLIENTID_BASE := android-tecno

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="vnd_kj8_h338-user 12 SP1A.210812.016 630886 release-keys"

BUILD_FINGERPRINT := TECNO/KJ8-OP/TECNO-KJ8:12/SP1A.210812.016/240607V930:user/release-keys
